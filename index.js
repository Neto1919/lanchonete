import { Op } from "sequelize"
//import db from "./db.js"
import Cozinha from "./Model/Cozinha.js"
import Garçom from "./Model/Garçom.js"
import Mesa from "./Model/Mesa.js"
import PainelC from "./Model/PainelC.js"
import PainelM from "./Model/PainelM.js"

async function criarCozinha() {
  await Cozinha.sync({ force: true});
  try {
    await Cozinha.create({
      comida: "Pizza"
    });

    await Cozinha.create({
      comida: "Aroiz"
    });

    await Cozinha.create({
      comida: "Batata"
    });

    await Cozinha.create({
      comida: "Feijão"
    });

    await Cozinha.create({
      comida: "Macarão"
    });
    
    console.log('Tuplas criadas com sucesso!');
  } catch (error) {
    console.error('Falha ao criar as tuplas:', error);
  } 
}

///Cozinha//

async function criarGarçom() {
  await Garçom.sync({ force: true});
  try {
    await Garçom.create({
      nome: "Cleito",
      idade: 21,
    });

    await Garçom.create({
      nome: "Junio",
      idade: 23,
    });

    await Garçom.create({
      nome: "João",
      idade: 21,
    });

    await Garçom.create({
      nome: "Issac",
      idade: 34,
    });

    await Garçom.create({
      nome: "Cauã",
      idade: 35,
    });

    console.log('Tuplas criadas com sucesso!');
  } catch (error) {
    console.error('Falha ao criar as tuplas:', error);
  }
}

//Garçom//

async function criarMesa() {
  await Mesa.sync({ force: true});
  try {
    await Mesa.create({
      numero: 1
    });

    await Mesa.create({
      numero: 2
    });

    await Mesa.create({
      numero: 3
    });

    await Mesa.create({
      numero: 4
    });

    await Mesa.create({
      numero: 5
    });

    console.log('Tuplas criadas com sucesso!');
  } catch (error) {
    console.error('Falha ao criar as tuplas:', error);
  }
}

//Mesa//

async function criarPainelC() {
  await PainelC.sync({ force: true});
  try {
    await PainelC.create({
      pedido: "Feijão, aroiz e batata"
    });

    await PainelC.create({
      pedido: "Feijão e aroiz"
    });

    await PainelC.create({
      pedido: "Pizza"
    });

    await PainelC.create({
      pedido: "Pizza e batata"
    });

    await PainelC.create({
      pedido: "Feijão e aroiz"
    });

    console.log('Tuplas criadas com sucesso!');
  } catch (error) {
    console.error('Falha ao criar as tuplas:', error);
  }
}

//PainelC//

async function criarPainelM() {
  await PainelM.sync({ force: true});
  try {
    await PainelM.create({
      pedido: "Feijão, aroiz e batata"
    });

    await PainelM.create({
      pedido: "Feijão e aroiz"
    });

    await PainelM.create({
      pedido: "Pizza"
    });

    await PainelM.create({
      pedido: "Pizza e batata"
    });

    await PainelM.create({
      pedido: "Feijão e aroiz"
    });

    console.log('Tuplas criadas com sucesso!');
  } catch (error) {
    console.error('Falha ao criar as tuplas:', error);
  }
}

//PainelM//

async function pesquisarCozinha() {
  console.log("Vai requisitar");
  Cozinha.findAll().then( (Cozinha) => {
   console.log(Cozinha); 
  });
  console.log("Algum texto");
}

async function pesquisarGarçom() {
  console.log("Vai requisitar");
  Garçom.findAll().then( (Garçom) => {
   console.log(Garçom); 
  });
  console.log("Algum texto");
}
async function pesquisarMesa() {
  console.log("Vai requisitar");
  Mesa.findAll().then( (Mesa) => {
   console.log(Mesa); 
  });
  console.log("Algum texto");
}
async function pesquisarPainelC() {
  console.log("Vai requisitar");
  PainelC.findAll().then( (PainelC) => {
   console.log(PainelC); 
  });
  console.log("Algum texto");
}
async function pesquisarPainelM() {
  console.log("Vai requisitar");
  PainelM.findAll().then( (PainelM) => {
   console.log(PainelM); 
  });
  console.log("Algum texto");
}

//crião//

/*criarCozinha();
criarGarçom();
criarMesa();
criarPainelC();
criarPainelM();*/

//Ditruição

Cozinha.destroy({
  where: {
    id: {
      [Op.eq]: 1
    }
  }
});

Garçom.destroy({
  where: {
    id: {
      [Op.eq]: 1
    }
  }
});
Mesa.destroy({
  where: {
    id: {
      [Op.eq]: 1
    }
  }
});
PainelC.destroy({
  where: {
    id: {
      [Op.eq]: 1
    }
  }
});
PainelM.destroy({
  where: {
    id: {
      [Op.eq]: 1
    }
  }
});

//Pesquisas//

pesquisarCozinha();
pesquisarGarçom();
pesquisarMesa();
pesquisarPainelC();
pesquisarPainelM();

