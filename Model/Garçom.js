import Sequelize from "sequelize";

import db from "../db.js";

const Garçom = db.define("garçom",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
      nome: {
      type: Sequelize.STRING,
      allowNull: false
    },
      idade: {
      type: Sequelize.INTEGER,
      allowNull: false
  }
 }
);

export default Garçom;