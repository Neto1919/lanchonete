import Sequelize from "sequelize";

import db from "../db.js";

const PainelC = db.define("painelC",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
      pedido: {
      type: Sequelize.STRING,
      allowNull: false
   }
  }
);

export default PainelC;