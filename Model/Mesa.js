import Sequelize from "sequelize";

import db from "../db.js";

const Mesa = db.define("mesa",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
      numero: {
      type: Sequelize.INTEGER,
      allowNull: false
  }
 }
);

export default Mesa;