import Sequelize from "sequelize";

import db from "../db.js";

const PainelM = db.define("painelm",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
      pedido: {
      type: Sequelize.STRING,
      allowNull: false
  }
 }
);

export default PainelM;