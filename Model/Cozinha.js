import Sequelize from "sequelize";

import db from "../db.js";

const Cozinha = db.define("cozinha",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
      comida: {
      type: Sequelize.STRING,
      allowNull: false
  }
 }       
);

export default Cozinha;